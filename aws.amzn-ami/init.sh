#!/bin/bash

# Import the atmos config files.
source /etc/atmos/atmos_base.conf
source /etc/atmos/atmos.conf

# add provisioning user
getent passwd $ANSIBLE_USER > /dev/null
if [ ! $? -eq 0 ]; then useradd $ANSIBLE_USER -m -c 'ansible provisioning user'; fi

if [ ! -e "/etc/sudoers.d/ansible" ]; then

echo ansible ALL = NOPASSWD: ALL > /tmp/ansible.sudoer.tmp
chmod 0440 /tmp/ansible.sudoer.tmp
mv /tmp/ansible.sudoer.tmp /etc/sudoers.d/ansible

fi

if [ ! -d "/etc/cron.register-server" ]; then mkdir /etc/cron.register-server; fi
cp files/register.sh /etc/cron.register-server/register.sh
cp files/updateAuthorizedKeys.sh /etc/cron.register-server/updateAuthorizedKeys.sh

cat >/etc/cron.d/register-server <<EOL
SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=root
* * * * * $ANSIBLE_USER /etc/cron.register-server/updateAuthorizedKeys.sh
* * * * * $ANSIBLE_USER /etc/cron.register-server/register.sh
EOL

#!/bin/bash

# Import the atmos config files.
source /etc/atmos/atmos_base.conf
source /etc/atmos/atmos.conf

INTERFACE_IPADDR=$(ifconfig $SERVER_DEVICE | grep 'inet addr:' | cut -d: -f2 | awk '{print $1}')

curl -X POST -H "Content-Type: application/json" -d '{ "resourceId": "'$SERVER_ID'", "ip": "'$INTERFACE_IPADDR'", "role": "'$SERVER_ROLE'", "groups": "'$SERVER_GROUPS'" }' http://$INVENTORY_HOST/servers

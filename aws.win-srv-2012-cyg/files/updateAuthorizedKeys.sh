#!/bin/bash

# Import the atmos config files.
source /etc/atmos/atmos_base.conf
source /etc/atmos/atmos.conf

PROVISIONER_AGENT_KEY=$(curl http://$INVENTORY_HOST/settings/agentPublicKey?inventory=$INVENTORY_ENV_TYPE)

mkdir -p ~/.ssh/
chmod 700 ~/.ssh
touch ~/.ssh/authorized_keys
chmod 600 ~/.ssh/authorized_keys

sed -i '/ssh-rsa.* ansible-provisioner/c \\' ~/.ssh/authorized_keys
sed -i '/^$/d' ~/.ssh/authorized_keys
echo $PROVISIONER_AGENT_KEY >> ~/.ssh/authorized_keys
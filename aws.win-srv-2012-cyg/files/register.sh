#!/bin/bash

# Import the atmos config files.
source /etc/atmos/atmos_base.conf
source /etc/atmos/atmos.conf

INTERFACE_IPADDR=$(netsh interface ip show config name="Ethernet" | grep "IP Address" | cut -d: -f2 | tr -d ' ')

curl -X POST -H "Content-Type: application/json" -d '{ "resourceId": "'$SERVER_ID'", "ip": "'$INTERFACE_IPADDR'", "role": "'$SERVER_ROLE'", "groups": "'$SERVER_GROUPS'" }' http://$INVENTORY_HOST/servers

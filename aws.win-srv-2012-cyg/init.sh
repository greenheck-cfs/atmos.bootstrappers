#!/bin/bash

# Import the atmos config files.
source /etc/atmos/atmos_base.conf
source /etc/atmos/atmos.conf

# add provisioning user
net user | grep " $ANSIBLE_USER " -q
if [ ! $? -eq 0 ]; then
net user $ANSIBLE_USER $ANSIBLE_USER_PASS /add /comment:"ansible provisioning user" /fullname:"ansible"
net localgroup Administrators ansible /add
sed -i "/^$ANSIBLE_USER/c \\" /etc/passwd
sed -i '/^$/d' /etc/passwd
mkpasswd -l -u $ANSIBLE_USER >> /etc/passwd
mkdir /home/$ANSIBLE_USER
chown $ANSIBLE_USER /home/$ANSIBLE_USER -R
fi

if [ ! -d "/etc/cron.register-server" ]; then mkdir /etc/cron.register-server; fi
cp files/register.sh /etc/cron.register-server/register.sh
cp files/updateAuthorizedKeys.sh /etc/cron.register-server/updateAuthorizedKeys.sh

cat >/etc/cron.d/register-server <<EOL
SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin:/cygdrive/c/windows:/cygdrive/c/windows/system32:/cygdrive/c/windows/system32/windowspowershell/v1.0
MAILTO=root
* * * * * $ANSIBLE_USER /etc/cron.register-server/updateAuthorizedKeys.sh
* * * * * $ANSIBLE_USER /etc/cron.register-server/register.sh
EOL

cygrunsrv -E cron
chmod 644 /etc/cron.d/*
chown SYSTEM:SYSTEM /etc/cron.d/*
cygrunsrv -S cron
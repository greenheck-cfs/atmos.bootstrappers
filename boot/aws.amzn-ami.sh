#!/bin/bash

# Parse arguments.
INVENTORY_HOST=
SERVER_ID=
SERVER_DEVICE=
SERVER_GROUPS=
INVENTORY_ENV_TYPE=
while getopts "h:i:d:g:e:" OPTION
do
    case $OPTION in
        h)
            INVENTORY_HOST=$OPTARG
            ;;
        i)
            SERVER_ID=$OPTARG
            ;;
        d)
            SERVER_DEVICE=$OPTARG
            ;;
        g)
            SERVER_GROUPS=$OPTARG
            ;;
        e)
            INVENTORY_ENV_TYPE=$OPTARG
            ;;
    esac
done

# Initialize atmos configuration directory.
mkdir -p /etc/atmos
touch /etc/atmos/atmos.conf

# Write default settings to the base configuration file.
cat >/etc/atmos/atmos_base.conf <<EOL

# The environment target to use when retrieving bootstrapper scripts.
ENV_TYPE=aws.amzn-ami

# The user to use for the server registration cron job.
ANSIBLE_USER=ansible

# The hostname where the inventory service is hosted.
INVENTORY_HOST=$INVENTORY_HOST
# The environment type to use for provisioning purposes (development, testing, production, etc)
INVENTORY_ENV_TYPE=$INVENTORY_ENV_TYPE

# The unique ID of this server's registration.
SERVER_ID=$SERVER_ID
# The device that is connected to the registration subnet.
SERVER_DEVICE=$SERVER_DEVICE
# Comma delimited.  Defines what groups to use for provisioning purposes.
SERVER_GROUPS=$SERVER_GROUPS

EOL

# Import the atmos config files.
source /etc/atmos/atmos_base.conf
source /etc/atmos/atmos.conf

cat >/etc/atmos/startup.sh <<EOL
#!/bin/bash
curl -o /tmp/atmos.bootstrappers.tar.gz https://bitbucket.org/greenheck-cfs/atmos.bootstrappers/get/master.tar.gz

if [ ! -d "/tmp/atmos.bootstrappers" ]; then mkdir /tmp/atmos.bootstrappers; fi
tar -zxvf /tmp/atmos.bootstrappers.tar.gz -C /tmp/atmos.bootstrappers --strip-components=1

cd /tmp/atmos.bootstrappers/$ENV_TYPE

./init.sh

rm /tmp/atmos.bootstrappers -r
rm /tmp/atmos.bootstrappers.tar.gz
EOL
chmod +x /etc/atmos/startup.sh

sed -i '/\/etc\/atmos\/startup.sh/c \\' /etc/rc.local
sed -i '/^$/d' /etc/rc.local
echo "/etc/atmos/startup.sh" >> /etc/rc.local

/etc/atmos/startup.sh
Param(
    [string]$h,
    [string]$i,
    [string]$d,
    [string]$g,
    [string]$q,
    [string]$r
)

$success = 0;
$attemptIndex = 1;

do
{
    Try
    {
        echo "Attempt $attemptIndex..."
        
        mkdir c:\tmp -Force
        
        Invoke-WebRequest http://cygwin.com/setup-x86_64.exe -OutFile c:\tmp\setup-x86_64.exe
        
        Start-Process "c:\tmp\setup-x86_64.exe" "-q -n -a x86_64 -R c:\cygwin64 -l c:\cygwin64_packages -s http://mirrors.kernel.org/sourceware/cygwin/" -wait
        Start-Sleep -s 1
        Start-Process "c:\tmp\setup-x86_64.exe" "-q -P nano" -wait
        Start-Sleep -s 1
        Start-Process "c:\tmp\setup-x86_64.exe" "-q -P curl" -wait
        Start-Sleep -s 1
        Start-Process "c:\tmp\setup-x86_64.exe" "-q -P openssh" -wait
        Start-Sleep -s 1
        Start-Process "c:\tmp\setup-x86_64.exe" "-q -P cron" -wait
        Start-Sleep -s 1
        Start-Process "c:\tmp\setup-x86_64.exe" "-q -P initscripts" -wait
        Start-Sleep -s 1
        Start-Process "c:\tmp\setup-x86_64.exe" "-q -P python" -wait
        Start-Sleep -s 1
        Start-Process "c:\tmp\setup-x86_64.exe" "-q -P rsync" -wait
        Start-Sleep -s 1
        
        cp c:\tmp\setup-x86_64.exe c:\cygwin64\setup.exe
        
        Invoke-WebRequest https://bitbucket.org/greenheck-cfs/atmos.bootstrappers/raw/master/boot/aws.win-srv-2012-cyg.sh -OutFile c:\tmp\aws.win-srv-2012-cyg.sh
        c:\cygwin64\bin\bash --login -c "/cygdrive/c/tmp/aws.win-srv-2012-cyg.sh -h $h -i $i -d $d -g $g -q $q -r $r"
        
        $success = 1
    }
    Catch [System.Exception]
    {
        #Log error (TODO)
        Sleep 30
    }
    Finally
    {
        $attemptIndex++;
    }
}
while (!$success -and $attemptIndex -lt 6)
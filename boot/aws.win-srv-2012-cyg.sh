#!/bin/bash

# Parse arguments.
INVENTORY_HOST=
SERVER_ID=
SERVER_DEVICE=
SERVER_GROUPS=
INVENTORY_ENV_TYPE=
while getopts "h:i:d:g:q:r:e:" OPTION
do
    case $OPTION in
        h)
            INVENTORY_HOST=$OPTARG
            ;;
        i)
            SERVER_ID=$OPTARG
            ;;
        d)
            SERVER_DEVICE=$OPTARG
            ;;
        g)
            SERVER_GROUPS=$OPTARG
            ;;
        q)
            ANSIBLE_USER_PASS=$OPTARG
            ;;
        r)
            CYGWIN_SERVER_USER_PASS=$OPTARG
            ;;
        e)
            INVENTORY_ENV_TYPE=$OPTARG
            ;;
    esac
done

# Initialize atmos configuration directory.
mkdir -p /etc/atmos
touch /etc/atmos/atmos.conf

# Write default settings to the base configuration file.
cat >/etc/atmos/atmos_base.conf <<EOL

# The environment target to use when retrieving bootstrapper scripts.
ENV_TYPE=aws.win-srv-2012-cyg

# The user to use for the server registration cron job.
ANSIBLE_USER=ansible
# The password to use for the ansible user.
ANSIBLE_USER_PASS=$ANSIBLE_USER_PASS

# The password to use for the backend cygwin service user.
CYGWIN_SERVER_USER=cyg_server
# The password to use for the backend cygwin service user.
CYGWIN_SERVER_USER_PASS=$CYGWIN_SERVER_USER_PASS

# The hostname where the inventory service is hosted.
INVENTORY_HOST=$INVENTORY_HOST
# The environment type to use for provisioning purposes (development, testing, production, etc)
INVENTORY_ENV_TYPE=$INVENTORY_ENV_TYPE

# The unique ID of this server's registration.
SERVER_ID=$SERVER_ID
# The device that is connected to the registration subnet.
SERVER_DEVICE=$SERVER_DEVICE
# Comma delimited.  Defines what groups to use for provisioning purposes.
SERVER_GROUPS=$SERVER_GROUPS

EOL

# Import the atmos config files.
source /etc/atmos/atmos_base.conf
source /etc/atmos/atmos.conf

cygrunsrv -R sshd
ssh-host-config --privileged --cygwin ntsec --port 22 --user $CYGWIN_SERVER_USER --pwd $CYGWIN_SERVER_USER_PASS -y
cygrunsrv -S sshd

powershell -Command "&Remove-NetFirewallRule -Name 'Allow SSH'"
powershell -Command "&New-NetFirewallRule -Name 'Allow SSH' -DisplayName 'Allow SSH' -Direction Inbound -Action Allow -Protocol TCP -LocalPort 22"

cygrunsrv -R cron
cron-config <<< each <<EOL
yes
ntsec
no
no
no
$CYGWIN_SERVER_USER_PASS
$CYGWIN_SERVER_USER_PASS
yes
EOL
cygrunsrv -R cron
cygrunsrv -I cron -d "CYGWIN cron" -u $CYGWIN_SERVER_USER -w $CYGWIN_SERVER_USER_PASS -p /usr/sbin/cron -a -n
cygrunsrv -S cron

mkdir -p /etc/cron.d
chmod 754 /etc/cron.d

cygrunsrv -R init
init-config -y
cygrunsrv -S init

cat >/etc/atmos/startup.sh <<EOL
#!/bin/bash
curl -o /tmp/atmos.bootstrappers.tar.gz https://bitbucket.org/greenheck-cfs/atmos.bootstrappers/get/master.tar.gz

if [ ! -d "/tmp/atmos.bootstrappers" ]; then mkdir /tmp/atmos.bootstrappers; fi
tar -zxvf /tmp/atmos.bootstrappers.tar.gz -C /tmp/atmos.bootstrappers --strip-components=1

cd /tmp/atmos.bootstrappers/$ENV_TYPE

./init.sh

rm /tmp/atmos.bootstrappers -r
rm /tmp/atmos.bootstrappers.tar.gz
EOL
chmod +x /etc/atmos/startup.sh

sed -i '/\/etc\/atmos\/startup.sh/c \\' /etc/rc.d/rc.local
sed -i '/^$/d' /etc/rc.d/rc.local
echo "/etc/atmos/startup.sh" >> /etc/rc.d/rc.local

/etc/atmos/startup.sh